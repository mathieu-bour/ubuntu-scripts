#!/bin/bash

# Remove older packages
apt purge -y 'php7*'
apt remove -y cmdtest 

# Setup APT Sources
apt update
apt install -y curl
add-apt-repository ppa:ondrej/php -y
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
apt update

# Install PHP, MySQL, NodeJS, Git and others
apt install -y --no-install-recommends php7.2 php7.2-mysql php7.2-curl php7.2-json php7.2-gd php7.2-msgpack php7.2-memcached php7.2-intl php7.2-sqlite3 php7.2-gmp php7.2-geoip php7.2-mbstring php7.2-redis php7.2-xml php7.2-zip php7.2-xdebug mysql-server mysql-client nodejs tilix gnome-tweak-tool git libxss1 libappindicator1 libindicator7 yarn

# Install Chrome
# ---------------------------
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome*.deb
apt install -f
rm google-chrome*.deb

# Install JetBrains IDEs
# ---------------------------
snap install phpstorm --classic
snap install webstorm --classic
snap install pycharm-professional --classic
snap install clion --classic

# NodeJS and Yarn
yarn global add n @angular/cli
n stable

# Composer
EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")"

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
rm composer-setup.php
mv composer.phar /usr/local/bin/composer
